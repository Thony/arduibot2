/*
 * startwarztones.h - Library for playing
 * StarWarz music with buzzer and led blink.
 * Created by A. MOSDINE, July 20, 2021.
 * Inspired from https://gist.github.com/nicksort/4736535.
 */
#include "Arduino.h"
#include "starwarztones.h"

// Tones Frequency
const uint16_t c = 261;
const uint16_t d = 294;
const uint16_t e = 329;
const uint16_t f = 349;
const uint16_t g = 391;
const uint16_t gS = 415;
const uint16_t a = 440;
const uint16_t aS = 466;
const uint16_t b = 494;
const uint16_t cH = 523;
const uint16_t cSH = 554;
const uint16_t dH = 587;
const uint16_t dSH = 622;
const uint16_t eH = 659;
const uint16_t fH = 698;
const uint16_t fSH = 740;
const uint16_t gH = 784;
const uint16_t gSH = 830;
const uint16_t aH = 880;

// Led blinking with tones
bool isLedDisplay = true;

void chorus1();
void chorus2();
void verse1();
void verse2();
void beep(uint16_t, uint16_t);
void blinkLed(uint16_t);

void playStarWarzTones() {
  isLedDisplay = true;
  chorus1();
  chorus2();
  verse1();
  chorus2();  
  verse2(); 
}

void chorus1()
{
	beep(a, 500);
	beep(a, 500);
	beep(a, 500);
	beep(f, 350);
	beep(cH, 150);
	beep(a, 500);
	beep(f, 350);
	beep(cH, 150);
	beep(a, 650);
	delay(500);
	beep(eH, 500);
	beep(eH, 500);
	beep(eH, 500);
	beep(fH, 350);
	beep(cH, 150);
	beep(gS, 500);
	beep(f, 350);
	beep(cH, 150);
	beep(a, 650);
	delay(500);
}

void chorus2()
{
  beep(aH, 500);
  beep(a, 300);
  beep(a, 150);
  beep(aH, 500);
  beep(gSH, 325);
  beep(gH, 175);
  beep(fSH, 125);
  beep(fH, 125);
  beep(fSH, 250);
  delay(325);
  beep(aS, 250);
  beep(dSH, 500);
  beep(dH, 325);
  beep(cSH, 175);
  beep(cH, 125);
  beep(b, 125);
  beep(cH, 250);
  delay(350);
}

void verse1()
{
  beep(f, 250);
  beep(gS, 500);
  beep(f, 350);
  beep(a, 125);
  beep(cH, 500);
  beep(a, 375);
  beep(cH, 125);
  beep(eH, 650);
  delay(500);
}

void verse2()
{
  beep(f, 250);
  beep(gS, 500);
  beep(f, 375);
  beep(cH, 125);
  beep(a, 500);
  beep(f, 375);
  beep(cH, 125);
  beep(a, 650);
  delay(650);
}

void beep(uint16_t note, uint16_t duration)
{
	tone(BUZZER_PIN, note, duration);
  blinkLed(duration);
	noTone(BUZZER_PIN);
  
	delay(50);
}

void blinkLed(uint16_t duration)
{
  // Led blink one time over two note
  isLedDisplay = !isLedDisplay;
  if (isLedDisplay)
  {
    digitalWrite(LED_PIN, HIGH);
		delay(duration);
		digitalWrite(LED_PIN, LOW);
  }
  else 
  {
    delay(duration);
  }
}
