/*
 * ArduiBot2.ino - Arduino robot DIY gift
 * StarWarz Robot playing music with buzzer and led blink.
 * Created by A. MOSDINE, July 20, 2021.
 */

#include "starwarztones.h"

bool isTriggerred = false;

// Ultrasonic distance sensor
const uint8_t TRIG_PIN = 8;
const uint8_t ECHO_PIN = 6;
const uint16_t SOUND_SPEED = 340; // m/s
const unsigned long TIMEOUT = 25000UL; // µs <=> (25000/1000000)*340 = 8.5m
const float TRIGGER_DISTANCE_MIN = 25.0; // cm

void setup()
{
  // Setup pin modes 
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);
  pinMode(BUZZER_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);

  // Debug only
  // Serial.begin(9600);
}

void loop()
{
  if(!isTriggerred && checkObstacle()) {
    isTriggerred = true;
    playStarWarzTones();
    isTriggerred = false;
  }
  
  delay(100);
}

bool checkObstacle()
{
  // Echo
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);
  
  // Compute distance
  const unsigned long elapsedTimeInMicroseconds = pulseIn(ECHO_PIN, HIGH, TIMEOUT);
  const float distanceInCentimeters = (elapsedTimeInMicroseconds/2.00) * 1.00e-6 * SOUND_SPEED * 100.00;

  // Debug only
  // Serial.println(elapsedTimeInMicroseconds);
  // Serial.println(distanceInCentimeters);

  // Distance = 0 <=> Detection problem
  if (distanceInCentimeters == 0.00)
    return false;

  return (distanceInCentimeters < TRIGGER_DISTANCE_MIN);
}
