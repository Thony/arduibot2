/*
 * startwarztones.h - Library for playing
 * StarWarz music with buzzer and led blink.
 * Created by A. MOSDINE, July 20, 2021.
 * Inspired from https://gist.github.com/nicksort/4736535.
 */
#ifndef starwarztones_h
#define starwarztones_h

#include "Arduino.h"

// Devices pin
const uint8_t BUZZER_PIN = 3;
const uint8_t LED_PIN = 13;

void playStarWarzTones();

#endif
